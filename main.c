#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <inttypes.h>
#include <setjmp.h>

#include "jpeglib.h"
#include "transupp.h"


typedef struct
{
  struct jpeg_destination_mgr pub; /* public fields */

  uint8_t* pBuffer; //output buffer, = argument pDestJpeg
  uint32_t uBufferSize; // output buffer size = puDestJpegSize
  uint32_t uOutSize; // real output size (will be lower than output buffer size)

} MemoryDestinationMgr;

void initMemoryDestinationMgr(j_compress_ptr cinfo)
{
  MemoryDestinationMgr* pMgr = (MemoryDestinationMgr*)cinfo->dest;

  pMgr->pub.next_output_byte = pMgr->pBuffer;
  pMgr->pub.free_in_buffer = pMgr->uBufferSize;
}

boolean emptyMemoryDestinationMgr(j_compress_ptr cinfo)
{
  /* There is no room left in the destination buffer, it is too small, returns error */
  return 0;
}

void termMemoryDestinationMgr(j_compress_ptr cinfo)
{
  /* The output buffer has been written, calculate the real size */
  MemoryDestinationMgr* pMgr = (MemoryDestinationMgr*)cinfo->dest;

  pMgr->uOutSize = pMgr->uBufferSize - pMgr->pub.free_in_buffer;
}

typedef struct
{
  struct jpeg_error_mgr pub; /* "public" fields */
  jmp_buf setjmp_buffer; /* for return to caller */

} ErrorMgr;

void exitErrorMgr (j_common_ptr cinfo)
{
  ErrorMgr* pMgr = (ErrorMgr*)cinfo->err;
  longjmp(pMgr->setjmp_buffer, 1);
}

int32_t CDKToolJpegCrop(uint8_t* pDestJpeg, uint32_t uDestJpegSize, const uint8_t* pSrcJpeg, uint32_t uSrcJpegSize, uint32_t ux, uint32_t uy, uint32_t uw, uint32_t uh )
{

  /* Source file JPEG struct */
  struct jpeg_decompress_struct srcInfo;
  j_decompress_ptr pSrcInfo = &srcInfo;

  /* Destination file JPEG struct */
  struct jpeg_compress_struct destInfo;
  j_compress_ptr pDestInfo = &destInfo;

  /* coef arrays */
  jvirt_barray_ptr * src_coef_arrays;
  jvirt_barray_ptr * dst_coef_arrays;

  /* error managers */
  ErrorMgr srcErrMgr, destErrMgr;

  /* transform options - for cropping */
  jpeg_transform_info transformOptions;

  /* output memory manager */
  MemoryDestinationMgr* pDestMemMgr;
  int32_t iRes = 0;


  if (setjmp(srcErrMgr.setjmp_buffer))
  {
    iRes = -2;
    goto labelEnd;
  }
  if (setjmp(destErrMgr.setjmp_buffer))
  {
    iRes = -1;
    goto labelEnd;
  }

  // setting error manager for Src
  srcInfo.err = jpeg_std_error(&srcErrMgr.pub);
  srcErrMgr.pub.error_exit = exitErrorMgr;

  // setting error manager for Dest
  destInfo.err = jpeg_std_error(&destErrMgr.pub);
  destErrMgr.pub.error_exit = exitErrorMgr;

  //creates Src and Dest structures
  jpeg_create_decompress(pSrcInfo);
  jpeg_create_compress(pDestInfo);

  // Read source file from memory buffer
  jpeg_mem_src(pSrcInfo, (unsigned char*)pSrcJpeg, uSrcJpegSize);
  jpeg_read_header(pSrcInfo, TRUE);

  if (ux + uw >= pSrcInfo->image_width || uy + uh >= pSrcInfo->image_height)
  {
    iRes = -3;
    goto labelEnd;
  }

  //setting transform options
  memset(&transformOptions, 0, sizeof(transformOptions));
  transformOptions.crop = 1;
  transformOptions.crop_width = uw;
  transformOptions.crop_height_set = JCROP_POS;
  transformOptions.crop_height = uh;
  transformOptions.crop_width_set = JCROP_POS;
  transformOptions.crop_xoffset = ux;
  transformOptions.crop_xoffset_set = JCROP_POS;
  transformOptions.crop_yoffset = uw;
  transformOptions.crop_yoffset_set = JCROP_POS;

  // calculate real crop coordinates
  jtransform_request_workspace(pSrcInfo, &transformOptions);

  /* Read source file as DCT coefficients */
  src_coef_arrays = jpeg_read_coefficients(pSrcInfo);

  /* Initialize destination compression parameters from source values */
  jpeg_copy_critical_parameters(pSrcInfo, pDestInfo);

  /* Adjust destination parameters if required by transform options;
   * also find out which set of coefficient arrays will hold the output.
   */
  dst_coef_arrays = jtransform_adjust_parameters(pSrcInfo, pDestInfo, src_coef_arrays, &transformOptions);

  //create destination memory manager : will return error if the buffer is too small
  pDestInfo->dest = (struct jpeg_destination_mgr *)(*pDestInfo->mem->alloc_small) ((j_common_ptr) pDestInfo, JPOOL_PERMANENT, sizeof(MemoryDestinationMgr));
  pDestMemMgr = (MemoryDestinationMgr*)pDestInfo->dest;
  pDestMemMgr->pBuffer = pDestJpeg;
  pDestMemMgr->uBufferSize = uDestJpegSize;
  pDestMemMgr->pub.init_destination = initMemoryDestinationMgr;
  pDestMemMgr->pub.empty_output_buffer = emptyMemoryDestinationMgr;
  pDestMemMgr->pub.term_destination = termMemoryDestinationMgr;
  pDestMemMgr->uOutSize = 0;

  /* Start compressor (note no image data is actually written here) */
  jpeg_write_coefficients(pDestInfo, dst_coef_arrays);

  /* Execute image transformation */
  jtransform_execute_transformation(pSrcInfo, pDestInfo, src_coef_arrays, &transformOptions);

  /* Finish compression and release memory */
  jpeg_finish_compress(pDestInfo);
  jpeg_finish_decompress(pSrcInfo);

  iRes = pDestMemMgr->uOutSize;

  labelEnd:
  jpeg_destroy_compress(pDestInfo);
  jpeg_destroy_decompress(pSrcInfo);

  return iRes;
}

int main(){
  

  FILE *fileptr;
  uint8_t *buffersrc;
  uint8_t *bufferdest;
  uint32_t filelen;

  fileptr = fopen("image.jpg", "rb");  // Open the file in binary mode
  fseek(fileptr, 0, SEEK_END);          // Jump to the end of the file
  filelen = ftell(fileptr);             // Get the current byte offset in the file
  rewind(fileptr);                      // Jump back to the beginning of the file

  buffersrc = (uint8_t *)malloc((filelen)*sizeof(uint8_t)); 

  bufferdest = (uint8_t *)calloc((filelen),sizeof(uint8_t));
  fread(buffersrc, filelen, 1, fileptr); // Read in the entire file
  fclose(fileptr); // Close the file

  uint32_t ux=0; uint32_t uy=5; uint32_t uw=5; uint32_t uh=5;

  CDKToolJpegCrop(bufferdest, filelen,buffersrc,filelen,ux,uy,uw,uh);
  for(uint32_t i=0;i<filelen;i++){
    printf("%x",buffersrc[i]);
  }

  printf("\n\n\nBufferdest");

  for(uint32_t i=0;i<filelen+1;i++){
    printf("%x",bufferdest[i]);
  }
  free(buffersrc);
  free(bufferdest);
}
